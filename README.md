## Module 4: AWS Storage Services

### Sub-task 1 - Create a static website

**1. Create a S3 Bucket**

![1.1](screenshots/1.1.png)

**2. Upload some files**

![1.2](screenshots/1.2.png)

**3. Set the correct permissions and make uploaded resources public**

![1.3.1](screenshots/1.3.1.png)

![1.3.2](screenshots/1.3.2.png)

**4. Access public uploaded resources via browser**

![1.4](screenshots/1.4.png)

**5. Enable cross-region replication for the bucket**

![1.5](screenshots/1.5.png)

### Sub-task 2 - Play with versioning

**1. Create another S3 Bucket**

![2.1](screenshots/2.1.png)

**2. Enable versioning for the bucket**

![2.2](screenshots/2.2.png)

**3. Upload files to the new bucket and make some changes**

![2.3.1](screenshots/2.3.1.png)

![2.3.2](screenshots/2.3.2.png)

![2.3.3](screenshots/2.3.3.png)

**4. Using AWS CLI, get the latest version of a specific file**

![2.4.1](screenshots/2.4.1.png)

![2.4.2](screenshots/2.4.2.png)

### Sub-task 3 - Practice more AWS CLI hacking and play with permissions

**1. Using AWS CLI list all the objects**

![3.1.1](screenshots/3.1.1.png)

![3.1.2](screenshots/3.1.2.png)

**2. Using different users try to upload new file/list all objects and view results**

![3.2.1](screenshots/3.2.1.png)

![3.2.2](screenshots/3.2.2.png)

### Sub-task 4 - Think a little bit

**1. Use cases for S3 I've seen on past projects**

- Utilizing Amazon S3 for hosting the static assets (HTML, CSS, JavaScript) of a frontend application for improved scalability and cost-effectiveness

- Using S3 to store and serve images for a full-stack application for wooden furniture store 

**2. Use cases for S3 I see reasonable**

- File Storage and Backup
- Data Archiving
- Static Website Hosting
- Content Delivery